const plan = require('flightplan');
const pkg = require('./package.json');

const username = 'deploy';
const tmpDir = pkg.name + '-' + new Date().getTime();

// configuration
plan.target('production', [
    {
        host: '128.199.223.84',
        username: username,
        agent: process.env.SSH_AUTH_SOCK
    },
]);

// run commands on localhost
plan.local((local) => {
    local.log('Copy files to remote hosts');
    var filesToCopy = local.exec('git ls-files', { silent: true });
    // rsync files to all the destination's hosts
    local.transfer(filesToCopy, '/tmp/' + tmpDir);
});

// run commands on remote hosts (destinations)
plan.remote((remote) => {
    remote.log('Move folder to root');
    remote.sudo('cp -R /tmp/' + tmpDir + ' ~', { user: username });
    remote.rm('-rf /tmp/' + tmpDir);

    remote.log('Install dependencies');
    remote.sudo('npm --production --prefix ~/' + tmpDir + ' install ~/' + tmpDir, { user: username });

    remote.log('Reload application');
    remote.sudo('ln -snf ~/' + tmpDir + ' ~/' + pkg.name, { user: username });
});