var serverStatus = require('express-server-status');
var express = require('express');
var url = require('url');
var _ = require('lodash');

var app = express();

app.set('port', (process.env.PORT || 8080));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.locals._ = _;

var middleware = {

    render: function (view) {
        return function (req, res, cb) {
            res.render(view);
        }
    },

    globalLocals: function (req, res, cb) {

        var url_parts = url.parse(req.url, true);
        var query = url_parts.query;
        // var thing = serverStatus(app);
        res.locals = {
            title: 'Ben Rhodes Digital',
            description: 'Personal Website of Ben Rhodes, a web developer from Melbourne',
            cssdirectory: 'css/',
            query: query,
            param: {}
        };

        //overwrite locals based on query string
        if ( query.devmode != undefined ) {
            res.locals.cssdirectory = 'devcss/';
        }

        cb();
    },

    index: function (req, res, cb) {
        res.locals.index_specific_variable = 'somethingindexspecific';
        res.locals.template = 'home';
        cb();
    },

    article: function (req, res, cb) {
        res.locals.article_specific_variable = 'some article specifc variables';
        res.locals.template = 'article';
        cb();
    }

};

app.use('/status', serverStatus(app));

app.use(middleware.globalLocals);
app.get('/', middleware.index, middleware.render('template/index'));
app.get('/home', middleware.index, middleware.render('template/index'));
app.get('/work', middleware.index, middleware.render('template/work'));
app.get('/info', middleware.index, middleware.render('template/index'));
app.get('/styleguide', middleware.index, middleware.render('template/styleguide'));
app.get('*', middleware.index, middleware.render('template/404'));

app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
});


